var boost_state = false;
var movie_tabId = -1;
var boost_controller_ip = "172.24.74.212:8000";
var ts_last_rq = -1;
var boostdb = null;
// prepare database
function prepareDatabase() {
	console.log("Initialize Database");
	var db = openDatabase('boostdb', '1.0', 'movie request db', 3*1024*1024);
	boostdb = db;
	db.transaction(function (tx) {
		tx.executeSql('DROP TABLE IF EXISTS reqtable');
		tx.executeSql('CREATE TABLE reqtable (serverip, notifyts, state, PRIMARY KEY(serverip))');
	});
	return db;
}

function enableBoostIcon(tabID){
	if(boost_state == false){
		chrome.browserAction.setIcon({path:"icon_red.png", tabId:tabID});
	}else{
		chrome.browserAction.setIcon({path:"icon_green.png", tabId:tabID});
	}
	chrome.browserAction.setTitle({title:"Click to toggle the BW Booster", tabId:tabID})
}
function disableBoostIcon(){
	chrome.browserAction.setIcon({path:"icon_grey.png"});	
}

function getHuluServerIP(){
	var server_ip = -1;
	var req = new XMLHttpRequest();  
	req.open('GET', 'http://127.0.0.1:8000/hulu.txt', false);   
	req.send(null);  
	if(req.status == 200){
		server_ip = req.responseText;
		if( server_ip.indexOf("macromedia-fcs") > -1){
			var server = server_ip.split("->");
			server = server[1];
			server = server.split(":");
			server_ip = server[0];
		}else{
			server_ip = -1;
		}
		console.log("hulu server: " + server_ip + " original resp: " + req.responseText);
	}
	return server_ip;
}

// Called when the url of a tab changes, if yes, initialize everything
function checkForMovieUrl(tabId, changeInfo, tab) {
	//@TODO: Only handle Netflix and Vudu  and hulu for now...
	if ( (tab.url.indexOf("netflix.com/WiPlayer") > -1) || (tab.url.indexOf("vudu.com/movies") > -1) ) {
		movie_tabId = tabId;
		console.log("tab url: " + tab.url + " movie_tabID: " + movie_tabId);
		// show the netflix action on that tab.
		enableBoostIcon(tabId);
		var db = prepareDatabase();
		addReporterToMovieTab(tab, db);
	}else if( (tab.url.indexOf("hulu.com") > -1) ){
        movie_tabId = tabId;
		console.log("tab url: " + tab.url + " movie_tabID: " + movie_tabId);
		enableBoostIcon(tabId);
		var db = prepareDatabase();
		addReporterToHuluTab(tab, db);
    }else{
		disableBoostIcon();
	}
	
};
function boosterTabChecker(highlightinfo){
	console.log("highting the tab with ID:" + highlightinfo.tabIds);
	var disableIcon = true;
	for(var i = 0; i < highlightinfo.tabIds.length; i++){
		tab_id = highlightinfo.tabIds[i];
		console.log("checking highlight tab id="+tab_id);
		if(tab_id == movie_tabId){
			console.log("enable the icon");
			enableBoostIcon(tab_id);
			disableIcon = false;
		}
	}
	if(disableIcon){
		disableBoostIcon();
	}
};
function boostClickHandler(tab){
	if(tab.id == movie_tabId){
		boost_state = !boost_state;
		console.log("boost status is now : " + boost_state);
		enableBoostIcon(tab.id);
	}
	if(boostdb != null){
		boostdb.transaction(function (tx) {
			tx.executeSql('SELECT * FROM reqtable', [], function (tx, results) {
				var len = results.rows.length;
				for (var i =0; i < len; i++) {
					var server_ip = results.rows.item(i).serverip;
					var state_string = "0";
					if( boost_state ){
						state_string = "1";
					}
					var request_string = "http://" + boost_controller_ip + "/_myboost/index.html?ip=" + escape(server_ip)+"&service=video&state="+escape(state_string);
					console.log("sending request:" + request_string);
					var xhr = new XMLHttpRequest();
					xhr.open("GET", request_string, true);
					xhr.send();
				}
			});
		});
	}
};

function notifyBoostController(request_string, db, serverip){
	var ts_now = Math.round(new Date().getTime() / 1000);
	db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM reqtable WHERE serverip=? and state=?', [serverip, boost_state], function (tx, results) {
			var len = results.rows.length;
			if(len != 0){
				var i = (results.rows.length-1);
				ts_last_rq =  results.rows.item(i).notifyts;
			}
			console.log("sql results " + results.rows.length + " ts_last_rq = " + ts_last_rq);
		});
	});
	console.log("last ts for (" + serverip + ", " + boost_state+") is "+ ts_last_rq + "now is " + ts_now);
	if( (ts_last_rq == -1) || (ts_now - ts_last_rq) > 3){
		//unix like timestamp (unit: second)
		ts_last_rq = ts_now;
		console.log("sending request:" + request_string);
		var xhr = new XMLHttpRequest();
		xhr.open("GET", request_string, true);
		xhr.send();

		//report to local storage
		db.transaction(function (tx) {
			tx.executeSql('SELECT * FROM reqtable WHERE serverip=?', [serverip], function (tx, results) {
				var len = results.rows.length;
				if( len == 0){
					console.log("insert serverip = " + serverip + "state = " + boost_state + " into database");
					tx.executeSql('INSERT INTO reqtable (serverip, notifyts, state) VALUES (?,?,?)', [serverip, ts_now, boost_state]);
				}else{
					console.log("update serverip = " + serverip + "state = " + boost_state + " into database");
					tx.executeSql('UPDATE reqtable SET notifyts=?, state=? WHERE serverip=?', [ts_now, boost_state, serverip]);
				}
			});
		});		

	}
}

function addReporterToMovieTab(tab, db){
	console.log("Report IP for tab: " + tab.id);
	chrome.webRequest.onResponseStarted.addListener(
		function(details){
			var server_ip = details.ip;
			var url = details.url;
			//@TODO: only handle netflix (isma/ismv) / vudu (flash)  for now ... 
			//only treat video/audio traffic specially
			if( url.indexOf("ismv") > -1 || url.indexOf("isma") > -1 || url.indexOf("flash") > -1){	
				//console.log("response started for \'"+ url + "\' from server (" + server_ip +")");
				var state_string = "0";
				if( boost_state ){
					state_string = "1";
				}
				var request_string = "http://" + boost_controller_ip + "/_myboost/index.html?ip=" + escape(server_ip)+"&service=video&state="+escape(state_string);
				notifyBoostController(request_string, db, server_ip);
			}else{
				//console.log("browser is requesting ... " + url);
			}
		},
		{"tabId":tab.id, urls: ["<all_urls>"]},
		[]
	);
}

//just for Hulu
function addReporterToHuluTab(tab, db){
	console.log("Report IP for tab: " + tab.id);
	chrome.webRequest.onResponseStarted.addListener(
		function(details){
			var server_ip = details.ip;
			var url = details.url;
			//@TODO: only handle netflix (isma/ismv) / vudu (flash)  for now ... 
			//only treat video/audio traffic specially
			if( url.indexOf("revenu") > -1 || url.indexOf("msg") > -1 || url.indexOf("sping") > -1 || url.indexOf("beacon") > -1){	
				//console.log("response started for \'"+ url + "\' from server (" + server_ip +")");
				var state_string = "0";
				if( boost_state ){
					state_string = "1";
				}
				var server_ip = getHuluServerIP();
				if( server_ip != -1){
					var request_string = "http://" + boost_controller_ip + "/_myboost/index.html?ip=" + escape(server_ip)+"&service=video&state="+escape(state_string);
					console.log("sending request:" + request_string);
					var xhr = new XMLHttpRequest();
					xhr.open("GET", request_string, true);
					xhr.send();

					//report to local storage
					var serverip = server_ip;
					db.transaction(function (tx) {
						tx.executeSql('SELECT * FROM reqtable WHERE serverip=?', [serverip], function (tx, results) {
							var len = results.rows.length;
							if( len == 0){
								console.log("insert serverip = " + serverip + "state = " + boost_state + " into database");
								tx.executeSql('INSERT INTO reqtable (serverip, notifyts, state) VALUES (?,?,?)', [serverip, ts_now, boost_state]);
							}else{
								console.log("update serverip = " + serverip + "state = " + boost_state + " into database");
								tx.executeSql('UPDATE reqtable SET notifyts=?, state=? WHERE serverip=?', [ts_now, boost_state, serverip]);
							}
						});
					});		

				}
			
			}else{
				//console.log("browser is requesting ... " + url);
			}
		},
		{"tabId":tab.id, urls: ["<all_urls>"]},
		[]
	);
}



// Listen for any changes to the URL of any tab.
chrome.tabs.onUpdated.addListener(checkForMovieUrl);
chrome.tabs.onHighlighted.addListener(boosterTabChecker);
chrome.browserAction.onClicked.addListener(boostClickHandler);
