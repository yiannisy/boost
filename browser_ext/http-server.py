#!/usr/bin/python
import SimpleHTTPServer
import SocketServer
import os

PORT = 8000

os.system("./lsof.sh &")

Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = SocketServer.TCPServer(("127.0.0.1", PORT), Handler)

print "serving at port", PORT
httpd.serve_forever()


print "kill lsof.sh"
os.system("kill -9 `ps -aux | grep lsof.sh | grep bash | awk '{print $2}'")
