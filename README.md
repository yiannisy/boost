Boost
==

A demostration for user-driven traffic prioritization for a home network setting (and ISP extensions).

The code and idea was developed during a hacathon at Cable Show 2012. Watch the related [video](https://www.youtube.com/watch?v=GqiDgmBPZsY).
It includes :

 * a chrome extension for user-initiated "boost" commands.
 * A pox module to enforce the relevant QoS settings on a home AP and upstream router.
 * Complementary materials (slides, etc)
